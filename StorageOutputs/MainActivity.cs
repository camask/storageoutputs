﻿using Android.App;
using Android.Widget;
using Android.OS;
using Java.Lang;
using System.IO;
using System;
using Android.Runtime;

namespace StorageOutputs
{
    [Activity (Label = "StorageOutputs", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        int count = 1;

        protected override void OnCreate (Bundle savedInstanceState)
        {
            base.OnCreate (savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView (Resource.Layout.main);

            // Get our button from the layout resource,
            // and attach an event to it
            var textview = FindViewById<TextView> (Resource.Id.textvieww);
            var bt = FindViewById<TextView> (Resource.Id.button);
            bt.Text = "Read File";
            bt.Click += (sender, e) => { 
                var documentsPath = Android.OS.Environment.ExternalStorageDirectory.ToString ();
                var filePath = Path.Combine (documentsPath, "RandomShit0");
                bt.Text = File.ReadAllText(filePath); };
            string strings = "";

            //foreach (var sds in ApplicationContext.GetExternalFilesDirs (null)) {
            //    strings += "\n E: GetExternalFilesDir: \n" + sds;
            //}
            var bt1 = FindViewById<TextView> (Resource.Id.button1);
            bt1.Click += (sender, e) => {for (int i = 0; i < 50; i++) {
                    SaveText ("RandomShit" + i, "Hej Henrik");
                } };
            bt1.Text = "Create Files";

            strings += "\n E: PackageResourcePath: \n" + ApplicationContext.PackageResourcePath;
            strings += "\n E: PackageCodePath: \n" + ApplicationContext.PackageCodePath;
            strings += "\n E: RootDirectory: \n" + Android.OS.Environment.RootDirectory;
            strings += "\n E: ExternalStorageDirectory: \n" + Android.OS.Environment.ExternalStorageDirectory;
            strings += "\n E: ExternalStorageDirectoryAbsolutePath: \n" + Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
            strings += "\n E: DataDirectory: \n" + Android.OS.Environment.DataDirectory;
            strings += "\n E: DataDirectoryAbsolutePath: \n" + Android.OS.Environment.DataDirectory.AbsolutePath;
            strings += "\n E: ExternalStorageState: \n" + Android.OS.Environment.ExternalStorageState;
            strings += "\n E: GetExternalStorageState: \n" + Android.OS.Environment.GetExternalStorageState(Android.OS.Environment.ExternalStorageDirectory);
            strings += "\n E: IsExternalStorageEmulated: \n" + Android.OS.Environment.IsExternalStorageEmulated;
            strings += "\n E: IsExternalStorageRemovable: \n" + Android.OS.Environment.IsExternalStorageRemovable;
            strings += "\n E: SECONDARY_STORAGE: \n" + JavaSystem.Getenv ("SECONDARY_STORAGE");
            strings += "\n E: EXTERNAL_SDCARD_STORAGE: \n" + JavaSystem.Getenv ("EXTERNAL_SDCARD_STORAGE");
            strings += "\n E: EXTERNAL_STORAGE: \n" + JavaSystem.Getenv ("EXTERNAL_STORAGE");
            strings += "\n E: EMULATED_STORAGE_SOURCE: \n" + JavaSystem.Getenv ("EMULATED_STORAGE_SOURCE");
            strings += "\n E: EMULATED_STORAGE_TARGET: \n" + JavaSystem.Getenv ("EMULATED_STORAGE_TARGET");

            strings += "\n Usable: " + (Android.OS.Environment.ExternalStorageDirectory.UsableSpace / 1024f) / 1024f +"Mb";
            textview.Text = strings;


        }

        public void SaveText (string filename, string text)
        {
          
            var documentsPath = Android.OS.Environment.ExternalStorageDirectory.ToString ();
            var filePath = Path.Combine(documentsPath, filename);
            System.IO.File.WriteAllText(filePath, text);
            //var s =  File.ReadAllText (filePath);
        }
    }
}


